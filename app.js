const express = require('express');
const knex = require('knex');
const app = express();
const port = process.env.PORT || 8000;

app.get('/api/genres', function(request, response) {
  let knex = connect();
  knex.select().from('genres').then((genres) => {
    response.json(genres);
  });
});

app.get('/api/genres/:id', function(request, response) {
  let knex = connect();
  let id = request.params.id;
  knex
    .select()
    .from('genres')
    .where('GenreId', id)
    .first()
    .then((genres) => {
      response.json(genres);
    });
});

function connect() {
  return require('knex')({
    client: 'sqlite3',
    connection: {
      filename: './database.sqlite'
    }
  });
}

app.listen(port, function() {
  console.log(`Listening on port ${port}`);
});
